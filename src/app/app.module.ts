import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { OnetwocolsComponent } from './onetwocols/onetwocols.component';

@NgModule({
  declarations: [
    AppComponent,
    OnetwocolsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
