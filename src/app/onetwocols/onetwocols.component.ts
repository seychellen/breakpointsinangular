import {Component, OnInit} from '@angular/core';
import {
   BreakpointObserver,
   BreakpointState
} from '@angular/cdk/layout';

@Component({
   selector: 'app-onetwocols',
   templateUrl: './onetwocols.component.html',
   styleUrls: ['./onetwocols.component.css']
})
export class OnetwocolsComponent implements OnInit {

   constructor(public breakpointObserver: BreakpointObserver) {
   }

   twoCols: boolean = false

   showNotizen = (col: number) => col == 0;
   showHandlungsempfehlungen = (col: number) => col == 0;
   showHinweise = (col: number) => col == 0;
   showProzessHistorie = (col: number) => col == 0;
   showAuflagen = (col: number) => (col == 1 && this.twoCols) || (col == 0 && !this.twoCols);
   showQESStatus = (col: number) => (col == 1 && this.twoCols) || (col == 0 && !this.twoCols);
   showAenderungshistorie = (col: number) => (col == 1 && this.twoCols) || (col == 0 && !this.twoCols);

   ngOnInit(): void {
      this.breakpointObserver
         .observe(['(min-width: 800.5px)'])
         .subscribe((state: BreakpointState) => {
            this.twoCols = state.matches;
         });
   }

}
