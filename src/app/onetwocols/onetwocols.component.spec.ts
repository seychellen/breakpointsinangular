import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnetwocolsComponent } from './onetwocols.component';

describe('OnetwocolsComponent', () => {
  let component: OnetwocolsComponent;
  let fixture: ComponentFixture<OnetwocolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnetwocolsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnetwocolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
